perftest
========

Profiling and performance tests, made like unit testing.

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ perftest

Use --help/-h to view info on the arguments::

    $ perftest --help

Release Notes
-------------

:0.0.1:
    Project created
